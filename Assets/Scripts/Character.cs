﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
	[SerializeField] private string characterName;
	[SerializeField] private string description;
	[SerializeField] private ElementType elemenent;

	public CharacterData characterData;

	private void Awake()
	{
		characterData = new CharacterData(characterName, description, elemenent);
	}

	void Update ()
	{
		UpdateCharacterData();
	}

	private void UpdateCharacterData()
	{
		if (characterName != characterData.Name || 
		    description != characterData.Description ||
		    elemenent.ToString() != characterData.Element
		)
		{
			characterData = new CharacterData(characterName, description, elemenent);
		}
	}
}
