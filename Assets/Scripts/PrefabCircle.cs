﻿using System.Collections.Generic;
using UnityEngine;

public class PrefabCircle : MonoBehaviour
{
	[SerializeField] private GameObject rotator;
	public List<GameObject> prefabs;

	private List<GameObject> spawnedPrefabs;
	private Vector3 originalCircleCenter;
	

	private void Awake()
	{
		spawnedPrefabs = new List<GameObject>();
	}

	void Start ()
	{
		originalCircleCenter = transform.position;
		CreatePrefabCircle();
	}

	public void RefreshPrefabCircle()
	{
		DestroyPrefabCircle();
		CreatePrefabCircle();
	}

	private void CreatePrefabCircle()
	{
		Vector3 tempPosition = transform.position;
		tempPosition.z += CircleService.CalculateRadius(prefabs.Count);
		transform.position = tempPosition;
		
		CircleService circle = new CircleService(rotator, prefabs.Count);
		List<Vector3> points = circle.CalculatePointPositions(transform);

		for(int i = 0; i < prefabs.Count; i++)
		{
			spawnedPrefabs.Add(Instantiate(prefabs[i], points[i], Quaternion.identity));
		}
	}

	private void DestroyPrefabCircle()
	{
		foreach (GameObject prefab in spawnedPrefabs)
		{
			Destroy(prefab);
		}
		spawnedPrefabs = new List<GameObject>();
		transform.position = originalCircleCenter;
	}

	public void RotatePrefabCircle(float degrees)
	{
		foreach (GameObject prefab in spawnedPrefabs)
		{
			prefab.transform.RotateAround(transform.position, Vector3.up, degrees);
			prefab.transform.rotation = Quaternion.identity;
		}
	}

	public Character GetCharacter(int index)
	{
		return spawnedPrefabs[index].GetComponent<Character>();
	}
}
