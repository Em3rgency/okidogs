﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

[RequireComponent(typeof(PrefabCircle))]
public class CharacterSelect : MonoBehaviour
{
	[SerializeField] private int rotationSteps = 50;
	[SerializeField] private GameObject detailsPanel;
	[SerializeField] private string nextScene;
	
	private PrefabCircle prefabCircle;
	
	private bool isRotating;
	private float rotationDegrees;
	private int currentStep, rotationDirection, selectedIndex;

	private void Awake()
	{
		prefabCircle = GetComponent<PrefabCircle>();
		ValidatePrefabCircleCharacters();
		rotationDegrees = 360 / prefabCircle.prefabs.Count;
	}

	private void Start()
	{
		UpdateDescriptionText();
	}

	private void Update()
	{
		HandleRotation();
	}

	private void HandleRotation()
	{
		if (isRotating)
		{
			prefabCircle.RotatePrefabCircle(rotationDegrees/rotationSteps * rotationDirection);
			currentStep++;
		}
		
		if (currentStep == rotationSteps)
		{
			currentStep = 0;
			isRotating = false;
			rotationDirection = 0;
		}
	}

	public void UI_Previous()
	{
		if (isRotating)
		{
			return;
		}

		isRotating = true;
		currentStep = 0;
		rotationDirection = -1;
		selectedIndex++;
		UpdateDescriptionText();
	}

	public void UI_Next()
	{
		if (isRotating)
		{
			return;
		}
		
		isRotating = true;
		currentStep = 0;
		rotationDirection = 1;
		selectedIndex--;
		UpdateDescriptionText();
	}

	public void UI_Confirm()
	{
		SceneManager.LoadScene(nextScene);
	}

	private void ValidatePrefabCircleCharacters()
	{
		foreach (GameObject prefab in prefabCircle.prefabs)
		{
			Character character = prefab.GetComponent<Character>();
			if (!character)
			{
				throw new Exception(prefab.name + " is missing Character script for CharacterSelect!");
			}
		}
	}

	private void UpdateDescriptionText()
	{
		Text name = detailsPanel.transform.Find("NameText").GetComponent<Text>();
		Text description = detailsPanel.transform.Find("DescriptionText").GetComponent<Text>();
		Text type = detailsPanel.transform.Find("TypeText").GetComponent<Text>();

		if (!name || !description || !type)
		{
			return;
		}
		
		int selectedCharacter = selectedIndex;
		int totalCharacters = prefabCircle.prefabs.Count;
		
		if (selectedCharacter < 0)
		{
			int remainder = selectedCharacter % totalCharacters;
			selectedCharacter = totalCharacters + remainder;
			if (remainder == 0)
			{
				selectedCharacter = 0;
			}
			
		} else if (selectedCharacter > 0)
		{
			selectedCharacter = selectedCharacter % totalCharacters;
		}

		Character character = prefabCircle.GetCharacter(selectedCharacter);
		
		name.text = character.characterData.Name;
		description.text = character.characterData.Description;
		type.text = character.characterData.Element;
	}
}
