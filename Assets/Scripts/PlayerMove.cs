﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(CharacterController))]
public class PlayerMove : MonoBehaviour {

    [SerializeField] private string horizontalInputName;
    [SerializeField] private string verticalInputName;
    [SerializeField] private string shootInputName;
    [SerializeField] private float movementSpeed, jumpStamina, staminaMax, staminaDrain, staminaRegen;

    private CharacterController charController;

    [SerializeField] private AnimationCurve jumpFallOff;
    [SerializeField] private float jumpMultiplier, runMultiplier;
    [SerializeField] private KeyCode jumpKey, runKey;


    private bool isJumping, isRunning, isMoving;
    private float staminaCurrent, movementSpeedDefault;

    public Slider staminaSlider;

    private void Awake()
    {
        charController = GetComponent<CharacterController>();
        staminaCurrent = staminaMax;
        movementSpeedDefault = movementSpeed;
    }

    private void Update()
    {
        Shoot();
        PlayerMovement();
        StaminaIncrease();
    }

    private void PlayerMovement()
    {
        float horizInput = Input.GetAxis(horizontalInputName);
        float vertInput = Input.GetAxis(verticalInputName);

        Vector3 rightMovement = transform.right * horizInput;
        Vector3 forwardMovement = transform.forward * vertInput;

        charController.SimpleMove(Vector3.ClampMagnitude(forwardMovement + rightMovement, 1.0f) * movementSpeed);

        if(charController.velocity.magnitude <= .1f)
        {
            isMoving = false;
        }
        else
        {
            isMoving = true;
        }

        JumpInput();
        RunInput();

    }

    private void JumpInput()
    {
        if (Input.GetKeyDown(jumpKey) && !isJumping)
        {
            isJumping = true;
            staminaCurrent -= jumpStamina;
            StartCoroutine(JumpEvent());
        }
    }

    private IEnumerator JumpEvent()
    {
        charController.slopeLimit = 90.0f; //This is so when jumping against walls character doesn't jiggle
        float timeInAir = 0.0f;

        do
        {
            float jumpForce = jumpFallOff.Evaluate(timeInAir);
            charController.Move(Vector3.up * jumpForce * jumpMultiplier * Time.deltaTime);
            timeInAir += Time.deltaTime;
            yield return null;
        } while (!charController.isGrounded && charController.collisionFlags != CollisionFlags.Above);

        charController.slopeLimit = 45.0f; //Setting back to default
        isJumping = false;
    }

    private void RunInput()
    {
        if (Input.GetKeyDown(runKey) && staminaCurrent > 0)
        {
            isRunning = true;
            movementSpeedDefault = movementSpeed;
            movementSpeed *= runMultiplier;
        }

        if (isRunning)
        {
            staminaCurrent -= (staminaDrain * Time.deltaTime);
        }

        if (!isMoving || staminaCurrent <= 0)
        {
            isRunning = false;
            movementSpeed = movementSpeedDefault;
        }

    }

    private void StaminaIncrease()
    {
        //FIXME: Put the slider UI back in
        return;
        staminaSlider.value = staminaCurrent;
        if(!isRunning && !isJumping && staminaCurrent < staminaMax)
        {
            staminaCurrent += staminaRegen * Time.deltaTime;
        }
        if(staminaCurrent > 100f)
        {
            staminaCurrent = 100f;
        }
    }

    private void Shoot()
    {
        //do shooting
    }

}
