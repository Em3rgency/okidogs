﻿public enum ElementType
{
    Water,
    Fire,
    Wood,
    Earth,
    Metal
}

public class CharacterData
{
    private string name, description;
    private ElementType element;

    public CharacterData(string name, string description, ElementType element)
    {
        this.name = name;
        this.description = description;
        this.element = element;
    }

    public string Name
    {
        get { return name; }
    }

    public string Description
    {
        get { return description; }
    }

    public string Element
    {
        get { return element.ToString(); }
    }
}
