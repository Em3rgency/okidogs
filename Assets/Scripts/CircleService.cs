﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor.Experimental.UIElements.GraphView;
using UnityEngine;

public class CircleService
{
    private GameObject rotator;
    private int numberOfPoints;
    
    public CircleService(GameObject rotator, int numberOfPoints)
    {
        this.rotator = rotator;
        this.numberOfPoints = numberOfPoints;
    }

    public List<Vector3> CalculatePointPositions(Transform centerTransform)
    {
        float degreeStep = (float) 360 / numberOfPoints;
        float radius = CalculateRadius(numberOfPoints);
        List<Vector3> points = new List<Vector3>();
        Vector3 originalRotatorPosition = rotator.transform.position;

        for (int i = 0; i < numberOfPoints; i++)
        {
            Vector3 tempPosition = rotator.transform.position;
            tempPosition.z += -radius;
            rotator.transform.position = tempPosition;
            rotator.transform.RotateAround(centerTransform.position, centerTransform.up, i * degreeStep);
            points.Add(rotator.transform.position);
            rotator.transform.position = originalRotatorPosition;
        }

        return points;
    }

    public static float CalculateRadius(int numberOfPoints)
    {
        return (float)numberOfPoints / 2;
    }
}
